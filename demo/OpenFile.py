#! /usr/bin/env python3
# coding: utf-8


import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

class OpenFile(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        hbox = QHBoxLayout()

        self.btn = QPushButton('openFile', self)
        self.btn.clicked.connect(self.openFile)
        hbox.addWidget(self.btn)

        self.textEdit = QTextEdit()
        # self.setCentralWidget(self.textEdit)
        hbox.addWidget(self.textEdit)

        self.setLayout(hbox)
        self.setGeometry(300, 300, 200, 300)
        self.setWindowTitle('openFIle')
        self.show()

    def openFile(self):
        filename, _ = QFileDialog.getOpenFileName(self, '/')
        self.textEdit.setText(filename)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = OpenFile()
    sys.exit(app.exec_())

