#!/usr/bin/env python3
# coding: utf-8

import sys
from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtGui import QIcon


class Example(QWidget):
    def __init__(self):
        super().__init__()

        self.initUI()


    def initUI(self):
        self.setGeometry(500, 400, 300, 300)
        self.setWindowTitle('IcoExample')
        self.setWindowIcon(QIcon('../static/ico.png'))

        self.show()

if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())