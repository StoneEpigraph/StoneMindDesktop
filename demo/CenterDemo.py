#! /usr/bin/env python3
# coding: utf-8

import sys
from PyQt5.QtWidgets import QWidget, QDesktopWidget, QApplication

class CenterDemo(QWidget):

    def __init__(self):

        super().__init__()
        self.initUI()


    def initUI(self):
        self.resize(400,750)

        self.center()
        self.setWindowTitle('CenterDemo')
        self.show()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = CenterDemo()

    sys.exit(app.exec_())