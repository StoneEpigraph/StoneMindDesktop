#! /usr/bin/env python3
# coding: utf-8

import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *


class PixmapDemo(QWidget):

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        hbox = QHBoxLayout(self)
        pixmap = QPixmap("../static/ico.png")

        lbl = QLabel(self)
        lbl.setPixmap(pixmap)

        hbox.addWidget(lbl)
        self.setLayout(hbox)

        self.move(300, 200)
        self.setWindowTitle('Red Rock')
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    pimxmap = PixmapDemo()
    sys.exit(app.exec_())