#! /usr/bin/env python3
# coding: utf-8


import sys
from PyQt5.QtWidgets import QMainWindow, QAction, qApp, QApplication
from PyQt5.QtGui import QIcon


class ToolBarDemo(QMainWindow):
    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):

        exitAction = QAction(QIcon('../static/ico.png'), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.triggered.connect(qApp.quit)

        self.toolBar = self.addToolBar('Exit')
        self.toolBar.addAction(exitAction)

        self.setGeometry(300, 300, 300, 300)
        self.setWindowTitle('ToolBar')
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    toolBar = ToolBarDemo()
    sys.exit(app.exec_())