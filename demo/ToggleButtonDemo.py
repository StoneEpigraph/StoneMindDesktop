#! /usr/bin/env python3
# coding: utf-8

import sys
from PyQt5.QtWidgets import QWidget, QPushButton, QFrame, QApplication
from PyQt5.QtGui import QColor


class ToggleButtonDemo(QWidget):

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.color = QColor(0, 0, 0)

        redb = QPushButton('Red', self)
        redb.setCheckable(True)
        redb.move(10, 10)
        redb.clicked[bool].connect(self.setColor)

        greenb = QPushButton('Green', self)
        greenb.setCheckable(True)
        greenb.move(10, 60)
        greenb.clicked[bool].connect(self.setColor)

        blueb = QPushButton('Blue', self)
        blueb.setCheckable(True)
        blueb.move(10, 110)
        blueb.clicked[bool].connect(self.setColor)

        self.square = QFrame(self)
        self.square.setGeometry(150, 20, 100, 100)
        self.square.setStyleSheet("QWidget { background-color: %s }" % self.color.name())

        self.setGeometry(300, 300, 280, 170)
        self.setWindowTitle('Toggle button')
        self.show()


    def setColor(self, pressed):
        source = self.sender()

        if pressed:
            value = 255
        else:
            value = 0

        if source.text() == "Red":
            self.color.setRed(value)
        elif source.text() == "Blue":
            self.color.setBlue(value)
        else:
            self.color.setGreen(value)


        self.square.setStyleSheet("QFrame { background-color: %s }" % self.color.name())


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = ToggleButtonDemo()
    sys.exit(app.exec_())