#! /usr/bin/env python3
# coding: utf-8

import webbrowser, sys
from PyQt5.QtWidgets import *


class ui_MainWindow(QWidget):
    item_name = "PyQt5打开外部链接"

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.tips1 = QLabel("网站： <a href='http://www.baidu.com'>百度</a>")
        self.tips1.setOpenExternalLinks(True)

        self.btn_webbrowser = QPushButton('webbrowser效果', self)
        self.btn_webbrowser.clicked.connect(self.btn_webbrowser_Clicked)

        grid = QGridLayout()
        grid.setSpacing(10)
        grid.addWidget(self.btn_webbrowser, 1, 0)
        grid.addWidget(self.tips1, 2, 0)

        self.setLayout(grid)
        self.resize(250, 150)
        self.setMinimumSize(266, 304)
        self.setMaximumSize(266, 304)
        self.center()
        self.setWindowTitle(self.item_name)
        self.show()

    def btn_webbrowser_Clicked(self):
        webbrowser.open('http://www.baidu.com')

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())


if __name__ == '__main__':
    qpp = QApplication(sys.argv)
    f = ui_MainWindow()
    sys.exit(qpp.exec_())
