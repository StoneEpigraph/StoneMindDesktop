#! /usr/bin/env python3
# coding: utf-8

import sys
from PyQt5.QtWidgets import QWidget, QApplication, QPushButton, QToolTip
from PyQt5.QtGui import QFont


class Example(QWidget):

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        QToolTip.setFont(QFont('SansSerif', 12))
        self.setToolTip('This is a <b>Qwidget</b> widget')
        btn = QPushButton('Button', self)
        btn.setToolTip('this is a btton')
        btn.resize(btn.sizeHint())
        btn.move(200,200)

        self.setGeometry(300,300,300,300)
        self.setWindowTitle('Tooltips')
        self.show()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())