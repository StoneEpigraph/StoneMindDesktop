#! /usr/bin/env python3
# coding: utf-8

from pinyin import PinYin

test = PinYin()
test.load_word()
print(test.hanzi2pinyin(string='钓鱼岛是中国的'))