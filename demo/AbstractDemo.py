#!/usr/bin/env python3
# coding: utf-8

import sys
from PyQt5.QtWidgets import QWidget, QLabel, QApplication


class AbstractDemo(QWidget):

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        lbOne = QLabel('one', self)
        lbOne.move(15,10)

        lbTwo = QLabel('two', self)
        lbTwo.move(35, 40)

        lbThree = QLabel('three', self)
        lbThree.move(55, 70)

        self.setGeometry(300,300,250,250)
        self.setWindowTitle('Absolute')
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = AbstractDemo()
    sys.exit(app.exec_())