#! /usr/bin/env python3
# coding: utf-8

from Tetrominoe import Tetrominoe
import random

class Shape():
    corrdsTable = (
        ((0, 0), (0, 0), (0, 0), (0, 0)),
        ((0, -1), (0, 0), (-1, 0), (-1, 1)),
        ((0, -1), (0, 0), (1, 0), (1, 1)),
        ((0, -1), (0, 0), (0, 1), (0, 2)),
        ((-1, 0), (0, 0), (1, 0), (0, 1)),
        ((0, 0), (1, 0), (0, 1), (1, 1)),
        ((-1, -1), (0, -1), (0, 0), (0, 1)),
        ((1, -1), (0, -1), (0, 0), (0, 1))
    )

    def __init__(self):
        self.corrds = [[0, 0] for i in range(4)]
        self.pieceShape = Tetrominoe.NoShape

        self.setShape(Tetrominoe.NoShape)

    def shape(self):
        return self.pieceShape

    def setShape(self, shape):
        table = Shape.corrdsTable[shape]

        for i in range(4):
            for j in range(2):
                self.corrds[i][j] = table[i][j]

        self.pieceShape = shape

    def setRandomShape(self):
        self.setShape(random.randint(1, 7))

    def x(self, index):
        return self.corrds[index][0]

    def y(self, index):
        return self.corrds[index][1]

    def setX(self, index, x):
        self.corrds[index][0] = x

    def setY(self, index, y):
        self.corrds[index][1] = y

    def minx(self):
        m = self.corrds[0][0]
        for i in range(4):
            m = min(m, self.corrds[i][0])
        return m

    def maxX(self):
        m = self.corrds[0][0]
        for i in range(4):
            m = max(m, self.corrds[i][0])
        return m

    def minY(self):
        m = self.corrds[0][1]
        for i in range(4):
            m = min(m, self.corrds[i][1])
        return m

    def maxY(self):
        m = self.corrds[0][1]
        for i in range(4):
            m = max(m, self.corrds[i][1])
        return m

    def rotateLeft(self):
        if self.pieceShape == Tetrominoe.SquareShape:
            return self

        result = Shape()
        result.pieceShape = self.pieceShape

        for i in range(4):
            result.setX(i, self.y(i))
            result.setY(i, -self.x(i))

        return result

    def rotateRight(self):
        if self.pieceShape == Tetrominoe.SquareShape:
            return self

        result = Shape()
        result.pieceShape = self.pieceShape

        for i in range(4):
            result.setX(i, -self.y(i))
            result.setY(i, self.x(i))
        return result





























