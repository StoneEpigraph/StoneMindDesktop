#!/usr/bin/env python3
# coding: utf-8

from PIL import Image
import os

class ImageUtil():

    def compressImage(self, srcPath, dstPath):
        for filename in os.listdir(srcPath):
            # 如果不存在目的目录则创建一个， 保持层级结构
            if not os.path.exists(dstPath):
                os.makedirs(dstPath)

            # 拼接完整的文件或文件夹路径
            srcFile = os.path.join(srcPath, filename)
            dstFile = os.path.join(dstPath, filename)
            print(srcFile)
            print(dstFile)

            # 如果是文件就处理
            if os.path.isfile(srcFile):
                # 打开原图片缩小保存
                sImg = Image.open(srcFile)
                w, h = sImg.size
                print(w, h)
                dImg = sImg.resize((w // 2, h // 2), Image.ANTIALIAS)
                dImg.save(dstFile)
                print(dstFile + " compressed succeeded")

            if os.path.isdir(srcFile):
                self.compressImage(srcFile, dstFile)


if __name__ == '__main__':
    imageUtil = ImageUtil()
    imageUtil.compressImage('/home/stone/Pictures/private', '/tmp/private')