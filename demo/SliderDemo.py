#! /usr/bin/env python3
# coding: utf-8

import sys
from PyQt5.QtWidgets import QWidget, QSlider, QLabel, QApplication
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap


class SliderDemo(QWidget):
    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        sld = QSlider(Qt.Horizontal, self)
        sld.setFocusPolicy(Qt.NoFocus)
        sld.setGeometry(30, 40, 100, 30)
        sld.valueChanged[int].connect(self.changeValue)

        self.label = QLabel(self)
        self.label.setPixmap(QPixmap('../static/volume/volume.ico'))
        self.label.setGeometry(160, 40, 330, 330)

        self.setGeometry(300, 300, 588, 470)
        self.setWindowTitle('QSlider')
        self.show()

    def changeValue(self, value):
        if value == 0:
            self.label.setPixmap(QPixmap('../static/volume/volume.ico'))
        elif value > 0 and value <= 30:
            self.label.setPixmap(QPixmap('../static/volume/min.ico'))
        elif value > 30 and value < 80:
            self.label.setPixmap(QPixmap('../static/volume/middle.ico'))
        else:
            self.label.setPixmap(QPixmap('../static/volume/max.ico'))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = SliderDemo()
    sys.exit(app.exec_())
