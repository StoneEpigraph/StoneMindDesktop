#! /usr/bin/env python3
# coding: utf-8

import time, os, sched

class ScheduleDemo():
    schedule = sched.scheduler(time.time, time.sleep)

    def perform_command(self, cmd, inc):
        os.system(cmd)

    def timeing_exec(self, cmd, inc = 60):
        self.schedule.enter(inc, 0, self.perform_command, (cmd, inc))
        self.schedule.run()


if __name__ == '__main__':
    print('show time after 10 seconds: ')
    schedule = ScheduleDemo()
    schedule.timeing_exec('date', 5)