#! /usr/bin/env python3
# coding: utf-8

import sys
from PyQt5.QtWidgets import QMainWindow, QApplication


class StatusDemo(QMainWindow):

    def __init__(self):
        super().__init__()

        self.initUI()


    def initUI(self):
        self.statusBar().showMessage('Ready')

        self.setGeometry(300,300,300,200)
        self.setWindowTitle('Status')
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    st = StatusDemo()

    sys.exit(app.exec_())