#! /usr/bin/env python3
# coding: utf-8

import sys
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QMainWindow

class ChangeSignal(QMainWindow):

    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):

        self.statusBar().showMessage('')

        self.setGeometry(300,300,250,150)
        self.setWindowTitle('Change Signal')
        self.show()

    def keyPressEvent(self, e):
        self.statusbBar().showMessage(e.key())
        if e.key() == Qt.Key_Escape:
            self.close()
        if e.key() == Qt.Key_Enter:
            self.statusBar().showMessage('Enter press')

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = ChangeSignal()
    sys.exit(app.exec_())