#! /usr/bin/env python
# coding: utf-8

import rsa


#生成一对密钥
(pubkey, privkey) = rsa.newkeys(256)


pub = pubkey.save_pkcs1()
pubfile = open('public.pem', 'w+')
pubfile.write(pub)
pubfile.close()

pri = privkey.save_pksc1()
prifile = open('private.pem', 'w+')
prifile.write(pri)
prifile.close()

# load公钥和密钥
message = 'hello'
with open('public.pem') as publicfile:
    p = publicfile.read()
    pubkey =rsa.PublicKey.load_pksc1(p)

with open('private.pem') as privatefile:
    p = privatefile.read()
    privkey = rsa.PrivateKey.load_pkcs1(p)

# 用公钥加密， 再用私钥解密
crypto = rsa.encrypt(message, pubkey)
message = rsa.decrypt(crypto, privkey)

print(message)

# sign 用私钥签名认证， 再用公钥验证签名
signature = rsa.sign(message, privkey, 'SHA-1')
rsa.verify('hello', signature, pubkey)

