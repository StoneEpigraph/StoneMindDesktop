#! /usr/bin/env python
# coding: utf-8

import sys
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

class App(QMainWindow):

    def __init__(self):
        super().__init__()
        self.initUI()

    # 窗口大小改变时动态改变子窗口的大小
    def resizeEvent(self, even):
        height = self.geometry().height() - self.toolBar.geometry().height() - self.statusBar().geometry().height()
        width = self.geometry().width()
        old_width = self.leftTree.geometry().width()
        toolBarHeight = self.toolBar.geometry().height()

        self.leftTree.setGeometry(0, toolBarHeight, old_width, height)
        self.rightArea.setGeometry(300, toolBarHeight, width - old_width, height)

    def initUI(self):

        # 布局
        self.layout = QGridLayout()

        # 工具栏
        self.toolBar = self.addToolBar("edit")

        # 同步
        syncAction = QAction(QIcon("static/sync.png"), "&sync", self)
        newNoteAction = QAction(QIcon("static/newNote.png"), '&newNote', self)
        self.toolBar.addAction(syncAction)
        self.toolBar.addAction(newNoteAction)

        # 菜单栏
        self.menuBar = QMenuBar(self)
        managerMenu = self.menuBar.addMenu("&manager")
        # 打开文件
        loginAction = QAction(QIcon(), '&login', self)
        managerMenu.addAction(loginAction)

        self.menuBar.addMenu(managerMenu)
        self.setMenuBar(self.menuBar)

        # 状态栏
        self.statusBar().showMessage("ready")


        #左侧树
        self.leftTree = QTreeWidget(self)
        rootItem = QTreeWidgetItem(self.leftTree)
        rootItem.setText(0, '笔记本组')
        item1 = QTreeWidgetItem(rootItem)
        item1.setText(1, '笔记本')
        mainHeight = self.geometry().height()
        self.leftTree.setGeometry(0, 0, 300, mainHeight)
        self.leftTree.setHeaderHidden(True)
        # self.leftTree.setSizeAdjustPolicy()
        self.layout.addWidget(self.leftTree, 1, 0)

        #右侧主窗口
        self.rightArea = QTextEdit(self)
        width = self.geometry().width()
        height = self.geometry().height()
        self.rightArea.setGeometry(300, 0, width, height)
        self.layout.addWidget(self.rightArea, 1, 1)
        self.rightArea.setText('12312312')
        self.rightArea.show()
        # self.setCentralWidget(self.rightArea)


        # main
        self.setGeometry(50, 50, 1024, 560)
        self.setWindowTitle('StoneMind')
        self.setWindowIcon(QIcon("static/fist.png"))
        self.show()





if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = App()
    sys.exit(app.exec_())